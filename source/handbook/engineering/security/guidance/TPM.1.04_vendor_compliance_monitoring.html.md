---
layout: handbook-page-toc
title: "TPM.1.04 - Vendor Compliance Monitoring Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# TPM.1.04 - Vendor Compliance Monitoring

## Control Statement

Maintain a program to monitor service providers’ compliance status at least annually.

## Context

We need to validate a third party's compliance status on a yearly basis to ensure they are also complying with compliance requirements. This will assist in obtaining new customers and help maintain assurance with our current customers. This control can be tested by reviewing the Procure to pay process in the handbook and vendor compliance monitoring process in the compliance repo runbook. 

## Scope

All third party service providers that fall within the GitLab Control Framework (GCF).

## Ownership

Control Owner:

* Security Compliance

Process Owner:

* Security Compliance

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Vendor Compliance Monitoring control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/925).

### Policy Reference

*  [Vendor compliance monitoring process in the handbook](/handbook/finance/procure-to-pay/#step-3-authorizations)
*  [Vendor compliance monitoring process in the Compliance repo runbook](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/runbooks/Vendor_Security_Report_Review.md)


## Framework Mapping

* PCI
  * 12.8.4
