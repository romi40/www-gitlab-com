---
layout: handbook-page-toc
title: "Library"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

The **Infrastructure Library** contains ***the current state* of our thinking about the problems we are solving**. This happens through two stages:

- First, we begin with **blueprints**, which define and scope a problem and provide options on how we might approach said problem.
- Second is the **design** stage, which fleshes out the approach and provides the technical design on the solution.

Once we have executed on the design, we update the relevant **architectural** and/or **operational** documentation. See the [Library Workflow](./workflow/) for more details.

## Directory

* [**Canary**](canary/) [`infra/5025`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5025)
* [**Chef Automation**](chef-automation/) [`infra/5078`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5078)
* [**CICD Omnibus**](cicd-pipeline/) [`release/framework/39`](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/39)
* [**Deployer**](deployer/) [`release/framework/40`](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/39)
* [**Disaster Recovery**](disaster-recovery/) [`infra/4741`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/4741)
* [**Infrastructure Git Workflow**](git-workflow/) [`infra/5276`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/4741)
* [**GitLab OKRs**](gitlab-okrs/) [`infra/6025`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/6025)
* [**Kubernetes Clusters Designations**](kubernetes-clusters-designations/) [`infra/6681`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/6681)
* [**Kubernetes Configuration**](kubernetes-configuration) [`gitlab-com/&64`](https://gitlab.com/groups/gitlab-com/-/epics/64)
* [**Kubernetes Traffic Transition**](kubernetes-transition-frontend-traffic/) [`infra/6673`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/6673)
* [**Merging CE and EE Codebases**](merge-ce-ee-codebases/) [`release/framework/nn`](https://gitlab.com/gitlab-com/gl-infra/delivery/)
* [**Terraform Automation**](terraform-automation/) [`infra/5079`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5079)
* [**PostgreSQL Database Bloat**](postgres-bloat/)[`infra/5924`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5924)
* [**PostgreSQL HA**](postgres-ha/)[`infra/5023`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5023)
* [**Scheduled Daily Deployments**](scheduled-daily-deployments/) [`gitlab-org/release/epics/13`](https://gitlab.com/groups/gitlab-org/release/-/epics/13)
* [**Security Fixes Development Location**](security-releases-development/) [`gitlab-org/gitlab-ce/issues/55648`](https://gitlab.com/gitlab-org/gitlab-ce/issues/55648)
* [**Service Inventory Catalog**](service-inventory-catalog/) [`infra/5926`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5926)
* [**Snowplow**](snowplow/) [`infra\4348`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/4348)
* [**Vault**](vault/) [`infra/epics/62`](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/62)
* [**ZFS Filesystem**](zfs-filesystem/)
* [**ZFS For Repository Storage nodes**](zfs-repo-storage/) [`gitlab-com/gl-infra/epics/65`](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/65)
* [**Deltas**](production/deltas/)
* [**Dogfooding CI/CD**](ci-cd/)
* [**OKRs**](okrs/)
* [**Planning Workflow**](planning/)
* [**Security Releases**](release/security/)
* [**PostgreSQL bloat**](database/postgres/bloat/)
* [**Service Levels and Error Budgets**](service-levels-error-budgets/)
* [**Repository Storage**](storage/block/repositories/)
* [**ZFS for Repository Storage Nodes**](storage/block/repositories/zfs-repo-storage/)
